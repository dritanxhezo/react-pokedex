import React, { Component } from 'react';
import pokedex_logo from './logo.svg';
import Pokelist from './components/pokelist/pokelist';
import Pokedetail from './components/pokedetail/pokedetail';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
        detail_url: "",
    };

    this.showDetail = this.showDetail.bind(this);
  }

  showDetail (url) {
      // e.preventDefault();
      // console.log("showing detail for: " + url);
      this.setState({detail_url: url});
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <p><img src={pokedex_logo} className="App-logo" alt="logo" /><span>Pokedex</span></p>
        </header>
        <Pokelist showDetail={this.showDetail}/>
        <Pokedetail detail_url={this.state.detail_url}/>
      </div>
    );
  }
}

export default App;
