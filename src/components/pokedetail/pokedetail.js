import React from "react";
import "./pokedetail.css";

class Pokedetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            url: "",
            detail: null,
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.detail_url && this.props.detail_url !== this.state.url) {
            this.setState({url: this.props.detail_url});
            this.refreshData(this.props.detail_url);
        }
    }

    componentDidMount() {
        this.refreshData(this.props.detail_url);
    }

    refreshData (url) {
        if (url) {
            fetch(url)
                .then(response => response.json())
                .then(data => this.setState({detail: data}));
        }
    }

    render() {
        if (this.state.detail) {
            return (
                <div className="poke-detail">
                    <h3>{this.state.detail.name}</h3>
                    <img src={this.state.detail.sprites.front_default} alt=""/>
                    <div className="info"><span>Height: </span><span>{this.state.detail.height}</span></div>
                    <div className="info"><span>Weight: </span><span>{this.state.detail.weight}</span></div>
                    <div className="info"><span>Species: </span><span>{this.state.detail.species.name}</span></div>
                    <div className="info"><span>Types: </span><ul className="taglist">{this.state.detail.types.map((t, index) => <li key={index}><span>{t.type.name}</span></li>)}</ul></div>
                    <div className="info"><span>Abilities: </span><ul className="taglist">{this.state.detail.abilities.map((a, index) => <li key={index}><span>{a.ability.name}</span></li>)}</ul></div>
                    <div className="info"><span>Moves: </span><ul className="taglist">{this.state.detail.moves.map((m, index) => <li key={index}><span>{m.move.name}</span></li>)}</ul></div>
                    <div className="info"><span>Held items: </span><ul className="taglist">{this.state.detail.held_items.map((i, index) => <li key={index}><span>{i.item.name}</span></li>)}</ul></div>
                </div>
            );
        }
        return null;
    }
}

export default Pokedetail;