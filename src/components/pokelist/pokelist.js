import React, { Component } from 'react';
import './pokelist.css';

const API = 'https://pokeapi.co/api/v2/';    // https://hn.algolia.com/api/v1/search?query=';
const DEFAULT_QUERY = 'pokemon';             //  redux';

class Pokelist extends Component {
    constructor(props) {
        super(props);

        this.state = {
            pokemons: [],
            prev_url: null,
            next_url: null,
        }
    }

    componentDidMount() {
        this.refreshData(API + DEFAULT_QUERY);
    }

    refreshData (url) {
        fetch( url )
            .then(response => response.json())
            .then(data => this.setState({ pokemons: data.results, prev_url: data.previous, next_url: data.next }));
    }

    render() {
        const pokemons = this.state.pokemons;
        return (
            <div className="poke-list">
                <ul>
                    {pokemons.map((pokemon, index) => <li key={index}><div className="pokeItem" onClick={() => this.props.showDetail(pokemon.url)}>{pokemon.name}</div></li>)}
                </ul>
                {this.state.prev_url ? <div className="prev"><button onClick={() => this.refreshData(this.state.prev_url)}>&#9665; Prev</button></div> : null}
                {this.state.next_url ? <div className="next"><button onClick={() => this.refreshData(this.state.next_url)}>Next &#9655;</button></div> : null}
            </div>
        );
    }
}

export default Pokelist;